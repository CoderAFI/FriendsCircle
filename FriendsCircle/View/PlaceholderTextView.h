//
//  PlaceholderTextView.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceholderTextView : UITextView {
    UIColor *_contentColor;
    BOOL _editing;
}

@property(strong, nonatomic) NSString *placeholder;
@property(strong, nonatomic) UIColor *placeholderColor;

@end
