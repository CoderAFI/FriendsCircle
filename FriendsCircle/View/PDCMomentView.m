//
//  PDCMomentView.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/10/13.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCMomentView.h"
#import "PDCMoment.h"
#import "PDCSudokuBoxView.h"
#import "NSString+PDC.h"
#import "UIImage+PDC.h"
#import "UIButton+ImageTitlePosition.h"
#import "NSNumber+PDC.h"
#import  "MLLinkLabel.h"
#import "PDCOperationMenu.h"
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>

#define TimeLineCellHighlightedColor [UIColor colorWithRed:92/255.0 green:140/255.0 blue:193/255.0 alpha:1.0]

static const CGFloat MomentViewCommonSpace = 15;

@interface PDCMomentView()<PDCSudokuBoxViewDelegate>

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) PDCMoment *moment;

@property (nonatomic, strong) UIImageView *userAvatarImageView;
@property (nonatomic, strong) UILabel *usernameLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) PDCSudokuBoxView *boxView;
@property (nonatomic, strong) UILabel *locationLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIButton *delButton;

@property (nonatomic, strong) UIButton *supportButton;
@property (nonatomic, strong) UIButton *commentButton;
@property (nonatomic, strong) UIButton *operationButton;
@property (nonatomic, strong) PDCOperationMenu *operationMenu;

@property (nonatomic, strong) MLLinkLabel *likeStatusLabel;

@property (nonatomic, strong) MASConstraint *supportButtonWidthConstraint;
@property (nonatomic, strong) MASConstraint *commentButtonWidthConstraint;

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@end

@implementation PDCMomentView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self buildUI];
        [self layoutUI];
        [self styleUI];
    }
    return self;
}

- (void)dealloc {
    self.tapGesture = nil;
}

#pragma mark - View

- (void)buildUI {
    __weak typeof(self) weakSelf = self;
    
    self.backgroundView = ({
        UIView * view = [[UIView alloc] initWithFrame:self.bounds];
        view.backgroundColor = [UIColor whiteColor];
        view;
    });

    self.userAvatarImageView = UIImageView.new;
    [self.contentView addSubview:self.userAvatarImageView];
    self.userAvatarImageView.userInteractionEnabled = YES;
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserAvatar)];
    [self.userAvatarImageView addGestureRecognizer:self.tapGesture];
    
    self.usernameLabel = UILabel.new;
    [self.contentView addSubview:self.usernameLabel];
    
    self.contentLabel = UILabel.new;
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:self.contentLabel];
    
    self.boxView = PDCSudokuBoxView.new;
    self.boxView.delegate = self;
    [self.contentView addSubview:self.boxView];
    
    self.locationLabel = UILabel.new;
    [self.contentView addSubview:self.locationLabel];
    
    self.timeLabel = UILabel.new;
    [self.contentView addSubview:self.timeLabel];
    
    self.delButton = UIButton.new;
    [self.delButton addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.delButton];
    
//    self.supportButton = UIButton.new;
//    [self.supportButton addTarget:self action:@selector(like) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentView addSubview:self.supportButton];
//
//    self.commentButton = UIButton.new;
//    [self.commentButton addTarget:self action:@selector(comment) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentView addSubview:self.commentButton];
    
    self.operationButton = [UIButton new];
    [self.operationButton setImage:[UIImage pdcImageNamed:@"AlbumOperateMore"] forState:UIControlStateNormal];
    [self.operationButton setImage:[UIImage pdcImageNamed:@"AlbumOperateMoreHL"] forState:UIControlStateHighlighted];
    [self.operationButton addTarget:self action:@selector(operationButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.operationButton];
    
    self.operationMenu = PDCOperationMenu.new;
    
    [self.operationMenu setLikeButtonClickedOperation:^{
        [UIView animateWithDuration:10 animations:^{
            [weakSelf.operationMenu mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@0);
            }];
        } completion:^(BOOL finished) {
            weakSelf.operationButton.hidden = NO;
            [weakSelf like];
        }];
    }];
    [self.operationMenu setCommentButtonClickedOperation:^{
        [UIView animateWithDuration:10 animations:^{
            [weakSelf.operationMenu mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@0);
            }];
        } completion:^(BOOL finished) {
            weakSelf.operationButton.hidden = NO;
            [weakSelf comment];
        }];
    }];
    [self.contentView addSubview:self.operationMenu];
    
    
    self.likeStatusLabel = MLLinkLabel.new;
    [self.likeStatusLabel setDidClickLinkBlock:^(MLLink *link, NSString *linkText, MLLinkLabel *label) {
        if (link.linkValue && weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(momentView:doTapLikeUser:ofMoment:)]) {
            [weakSelf.delegate momentView:weakSelf doTapLikeUser:link.linkValue ofMoment:weakSelf.moment];
        }
    }];
    self.likeStatusLabel.allowLineBreakInsideLinks = YES;
    self.likeStatusLabel.dataDetectorTypesOfAttributedLinkValue = MLDataDetectorTypeNone;
    [self.contentView addSubview:self.likeStatusLabel];
}

- (void)layoutUI {
    [self.userAvatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(MomentViewCommonSpace);
        make.top.equalTo(self.contentView).with.offset(MomentViewCommonSpace);
        make.size.mas_equalTo(CGSizeMake(34, 34));
    }];
    
    [self.usernameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(57);
        make.centerY.equalTo(self.userAvatarImageView);
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.usernameLabel);
        make.top.equalTo(self.usernameLabel.mas_bottom).with.offset(10);
        make.right.equalTo(self.contentView).offset(-MomentViewCommonSpace).priorityLow();
    }];
    
    [self.likeStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.usernameLabel);
        make.right.equalTo(self.contentView).offset(-MomentViewCommonSpace);
        make.bottom.equalTo(self.contentView).offset(-MomentViewCommonSpace);
    }];
    
    [self.locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.timeLabel.mas_top).offset(-10);
        make.left.equalTo(self.usernameLabel);
    }];
    
    [self.delButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.timeLabel.mas_right).with.offset(10);
        make.centerY.equalTo(self.timeLabel);
    }];
    
    [self.operationButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-MomentViewCommonSpace);
        make.centerY.equalTo(self.delButton);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    [self.operationMenu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-MomentViewCommonSpace - 25);
        make.centerY.equalTo(self.delButton);
        make.width.equalTo(@0);
        make.height.equalTo(@30);
    }];
    
//    [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.contentView).offset(-MomentViewCommonSpace);
//        make.height.equalTo(@20);
//        make.centerY.equalTo(self.delButton);
//    }];
//
//    [self.supportButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.commentButton.mas_left).with.offset(-MomentViewCommonSpace);
//        make.height.equalTo(@20);
//        make.centerY.equalTo(self.delButton);
//    }];
}

- (void)styleUI {
    
    self.usernameLabel.textColor = UIColorFromRGB(0x5682B2);
    self.usernameLabel.font = [UIFont boldSystemFontOfSize:16];
    self.usernameLabel.textAlignment = NSTextAlignmentLeft;
    
    self.contentLabel.textColor = UIColorFromRGB(0x464646);
    self.contentLabel.font = [UIFont systemFontOfSize:14];
    CGFloat preferredMaxWidth = UIScreen.mainScreen.bounds.size.width - MomentViewCommonSpace - 57;
    self.contentLabel.preferredMaxLayoutWidth = preferredMaxWidth;
    [self.contentLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
    self.locationLabel.textColor = UIColorFromRGB(0x5682B2);
    self.locationLabel.font = [UIFont systemFontOfSize:12];
    self.locationLabel.textAlignment = NSTextAlignmentLeft;
    
    self.timeLabel.textColor = UIColorFromRGB(0x999999);
    self.timeLabel.font = [UIFont systemFontOfSize:11];
    
    [self.delButton setTitle:PDCLocalizedString(@"moment_delete") forState:UIControlStateNormal];
    [self.delButton setTitleColor:UIColorFromRGB(0x5682B2) forState:UIControlStateNormal];
    self.delButton.titleLabel.font = [UIFont systemFontOfSize:12];
    
//    [@[self.commentButton, self.supportButton] enumerateObjectsUsingBlock:^(UIButton *  _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
//        button.layer.cornerRadius = 10;
//        button.layer.borderColor = UIColorFromRGB(0xCCCCCC).CGColor;
//        button.layer.borderWidth = 1;
//        button.layer.masksToBounds = YES;
//        button.titleLabel.font = [UIFont systemFontOfSize:12];
//        [button setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
//    }];
    
//    [self.supportButton setImage:[UIImage pdcImageNamed:@"question_like_normal"] forState:UIControlStateNormal];
//    [self.supportButton setImage:[UIImage pdcImageNamed:@"question_like_normal"] forState:UIControlStateSelected];
//    [self.supportButton setTitleColor:TimeLineCellHighlightedColor forState:UIControlStateSelected];
    
//    [self.commentButton setImage:[UIImage pdcImageNamed:@"reply"] forState:UIControlStateNormal];
    
    self.likeStatusLabel.font = [UIFont systemFontOfSize:14];
    self.likeStatusLabel.linkTextAttributes = @{NSForegroundColorAttributeName : TimeLineCellHighlightedColor};
    self.likeStatusLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.likeStatusLabel.allowLineBreakInsideLinks = YES;
}

- (void)updateConstraints {
    [self.supportButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:3];
    [self.commentButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleLeft imageTitleSpace:3];
    [super updateConstraints];
}

#pragma mark - PDCSudokuBoxView Delegate

- (void)sudoKuBoxView:(PDCSudokuBoxView *)boxView didTapImageViewAtIndex:(NSInteger)imageIndex {
    if (self.delegate && [self.delegate respondsToSelector:@selector(momentView:doTapBoxView:withMoment:andImageIndex:)]) {
        [self.delegate momentView:self doTapBoxView:self.boxView withMoment:self.moment andImageIndex:imageIndex];
    }
}

#pragma mark - Data

- (void)bindModel:(PDCMoment *)moment withIndex:(NSInteger)index {
    self.moment = moment;
    self.index = index;
    
    // MARK:: USER
    NSURL *imageURL = [NSURL URLWithString:moment.createUser.avatarImageUrl];
    [self.userAvatarImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage pdcImageNamed:@"user_default"]];
    self.usernameLabel.text = moment.createUser.nickName;
    
    // MARK:: CONTENT
    self.contentLabel.text = moment.content;
    
    if (self.moment.imageUrls.count > 0) {
        self.boxView.hidden = NO;
        self.boxView.userInteractionEnabled = YES;
        self.boxView.itemsSource = self.moment.imageUrls;
        [self.boxView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset(57);
            make.right.equalTo(self.contentView).with.offset(-15).priorityLow();
            make.top.equalTo(self.contentLabel.mas_bottom).offset(MomentViewCommonSpace);
            make.bottom.equalTo(self.timeLabel.mas_top).offset(-MomentViewCommonSpace).priorityLow();
        }];
    } else {
        self.boxView.itemsSource = nil;
        self.boxView.hidden = YES;
        self.boxView.userInteractionEnabled = NO;
    }
    
    // MARK: Location
    self.locationLabel.hidden = (moment.location.length == 0);
    self.locationLabel.text = moment.location;
    
    // MARK::BOTTOM
    self.timeLabel.text = [moment.createTimestamp showTimeString];
    
    if (moment.likeNum.integerValue > 0) {
        NSString *likeNumStr = moment.likeNum.stringValue;
        [self.supportButton setTitle:likeNumStr forState:UIControlStateNormal];
    } else {
        [self.supportButton setTitle:PDCLocalizedString(@"moment_like") forState:UIControlStateNormal];
    }
    
    if (moment.currentUserIsLike) {
        self.supportButton.layer.borderColor = TimeLineCellHighlightedColor.CGColor;
        self.supportButton.selected = YES;
    } else {
        self.supportButton.layer.borderColor = UIColorFromRGB(0xCCCCCC).CGColor;
        self.supportButton.selected = NO;
    }
    
    CGFloat supportButtonWidth = [self dynamicImageButtonWidth:self.supportButton.titleLabel.text];
    [self.supportButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(supportButtonWidth);
    }];
    if (moment.commentNum.integerValue > 0) {
        NSString *commentNumStr = moment.commentNum.stringValue;
        [self.commentButton setTitle:commentNumStr forState:UIControlStateNormal];
    } else {
        [self.commentButton setTitle:PDCLocalizedString(@"moment_comment") forState:UIControlStateNormal];
    }
    CGFloat commentButtonWidth = [self dynamicImageButtonWidth:self.commentButton.titleLabel.text];
    [self.commentButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(commentButtonWidth);
    }];

    self.delButton.hidden = !moment.canDel;
    
    if (moment.likes.count > 0) {
        self.likeStatusLabel.hidden = NO;
        NSTextAttachment *attach = [NSTextAttachment new];
        attach.image = [UIImage pdcImageNamed:@"Like"];
        attach.bounds = CGRectMake(0, -3, 16, 16);
        NSMutableAttributedString *likeIconLinkStr = [NSMutableAttributedString attributedStringWithAttachment:attach].mutableCopy;
        UIColor *highLightColor = [UIColor blueColor];
        [likeIconLinkStr setAttributes:@{NSForegroundColorAttributeName : highLightColor} range:NSMakeRange(0, 0)];
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:likeIconLinkStr];
        NSArray<PDCLike *> *likes = moment.likes;
        NSInteger i = 0;
        for (PDCLike *likeModel in likes) {
            if (i > 0) {
                [attributedText appendAttributedString:[[NSAttributedString alloc] initWithString:@", "]];
            }
            if (!likeModel.attributedContent) {
                likeModel.attributedContent = [PDCMomentView generateAttributedStringWithLikeModel:likeModel];
            }
            [attributedText appendAttributedString:likeModel.attributedContent];
            i++;
        }
        self.likeStatusLabel.attributedText = [attributedText copy];
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.usernameLabel);
            make.bottom.equalTo(self.likeStatusLabel.mas_top).offset(-10);
        }];
        CGFloat likeContentHeight = [PDCMomentView likeContentHeightForMoment:self.moment];
        [self.likeStatusLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(likeContentHeight);
        }];
    } else {
        self.likeStatusLabel.hidden = YES;
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.usernameLabel);
            make.bottom.equalTo(self.contentView).offset(-MomentViewCommonSpace);
        }];
        [self.likeStatusLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
    }
    
    if (self.index == -1) {
        self.supportButton.hidden = YES;
        self.delButton.hidden = YES;
        self.commentButton.hidden = YES;
        self.likeStatusLabel.hidden = YES;
    }
}

#pragma mark - Control

- (void)delete {
    if (self.delegate && [self.delegate respondsToSelector:@selector(momentView:doDeleteMoment:)]) {
        [self.delegate momentView:self doDeleteMoment:self.moment];
    }
}

- (void)like {
    if (self.delegate && [self.delegate respondsToSelector:@selector(momentView:doLikeMoment:)]) {
        [self.delegate momentView:self doLikeMoment:self.moment];
    }
}

- (void)comment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(momentView:doCommentMoment:)]) {
        [self.delegate momentView:self doCommentMoment:self.moment];
    }
}

- (void)tapUserAvatar {
    if (self.delegate && [self.delegate respondsToSelector:@selector(momentView:doTapUserAvatarOfMoment:)]) {
        [self.delegate momentView:self doTapUserAvatarOfMoment:self.moment];
    }
}

- (void)operationButtonClicked {
    self.operationButton.hidden = !self.operationButton.hidden;
    
    if (self.operationButton.hidden) {
        [UIView animateWithDuration:10 animations:^{
            [self.operationMenu mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@181);
            }];
        }];
    } else {
        [UIView animateWithDuration:10 animations:^{
            [self.operationMenu mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@0);
            }];
        }];
    }
}

#pragma mark - Util

+ (CGFloat)heightForModel:(PDCMoment *)moment {
    NSString *username = moment.createUser.nickName;
    NSString *content = moment.content;
    NSString *showCreateTimeStr = [moment.createTimestamp showTimeString];

    CGFloat usernameLabelHeight = [username sizeWithFont:[UIFont boldSystemFontOfSize:16]].height;
    
    CGFloat dynamicContentLabelHeight = [content sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(SCREEN_WIDTH - MomentViewCommonSpace - 57, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping].height;
    
    CGFloat locationHeight = [moment.location sizeWithFont:[UIFont systemFontOfSize:12]].height;
    
    CGFloat timeLabelHeight = [showCreateTimeStr sizeWithFont:[UIFont systemFontOfSize:11]].height;
    
    dynamicContentLabelHeight += MomentViewCommonSpace * 2;
    
    if (moment.imageUrls.count > 0) {
        dynamicContentLabelHeight +=  [PDCSudokuBoxView heightForItemsSource:moment.imageUrls width:(SCREEN_WIDTH - MomentViewCommonSpace - 57)];
        dynamicContentLabelHeight += MomentViewCommonSpace;
    }
    
    if (moment.location.length > 0) {
        dynamicContentLabelHeight += (locationHeight + 10);
    }
    
    if (moment.likes.count > 0) {
        CGFloat likeContentHeight = [PDCMomentView likeContentHeightForMoment:moment];
        dynamicContentLabelHeight += (likeContentHeight + 10);
    }
    
    return MomentViewCommonSpace + usernameLabelHeight + dynamicContentLabelHeight + timeLabelHeight + MomentViewCommonSpace;
}

- (CGFloat)dynamicImageButtonWidth:(NSString *)text {
    return [text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(MAXFLOAT, 20)].width + 12 + 20;
}

+ (NSMutableAttributedString *)generateAttributedStringWithLikeModel:(PDCLike *)model {
    NSString *text = model.createdUser.nickName;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
    UIColor *highLightColor = [UIColor blueColor];
    [attString setAttributes:@{NSForegroundColorAttributeName : highLightColor, NSLinkAttributeName : model.createdUser.userId} range:[text rangeOfString:model.createdUser.nickName]];
    
    return attString;
}

+ (CGFloat)likeContentHeightForMoment:(PDCMoment *)moment {
    NSMutableString *likesContentStr = [[NSMutableString alloc] initWithString:@"🐻"];
    NSArray<PDCLike *> *likes = moment.likes;
    NSInteger i = 0;
    for (PDCLike *likeModel in likes) {
        if (i > 0) {
            [likesContentStr appendString:@", "];
        }
        [likesContentStr appendString:likeModel.createdUser.nickName];
        i++;
    }
    return [likesContentStr sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(SCREEN_WIDTH - MomentViewCommonSpace - 57, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping].height;
}

@end
