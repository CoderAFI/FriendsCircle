//
//  PDCOperationMenu.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/11.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDCOperationMenu : UIView

@property (nonatomic, assign, getter = isShowing) BOOL show;

@property (nonatomic, copy) void (^likeButtonClickedOperation)(void);
@property (nonatomic, copy) void (^commentButtonClickedOperation)(void);

@end
