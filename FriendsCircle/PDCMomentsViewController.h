//
//  FriendsCircleViewController.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/15.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCMoment.h"

typedef NS_ENUM(NSInteger, MomentsShowMode) {
    AllFriends,
    MineOwn
};

@class PDCMomentsViewController;

@protocol PDCMomentsViewControllerDelegate<NSObject>

#pragma mark - Data Delegate

@required
- (BOOL)isUserLoggedIn;
- (void)dataLoadMoments:(BOOL)isMore;

#pragma mark - Action Delegate

@required
- (void)actionAddNewMoment;
- (void)actionUserAvatarClick;
- (void)actionHeaderImageViewClick;
- (void)actionDeleteMoment:(PDCMoment *)moment;

@optional

- (void)actionLikeMoment:(PDCMoment *)moment;
- (void)actionClickUserAvatarOfMoment:(PDCMoment *)moment;
- (void)actionClickLikeUser:(NSString *)userId ofMoment:(PDCMoment *)moment;
- (void)actionCommentMoment:(PDCMoment *)moment withContent:(NSString *)content;
- (void)actionDeleteComment:(PDCComment *)comment ofMoment:(PDCMoment *)moment;

@end

@interface PDCMomentsViewController : UIViewController

@property (nonatomic, assign) BOOL containsInTabController;
@property (nonatomic, assign) MomentsShowMode showMode;

@property (nonatomic, strong) UITableView *friendsCircleListView;
@property (nonatomic, strong) NSMutableArray<PDCMoment *> *friendsCircleListItems;

@property (nonatomic, weak) id<PDCMomentsViewControllerDelegate> delegate;

- (void)updateListViewHeader:(NSString *)backgroundImageUrl userAvatarImage:(NSString *)avatarImageUrl andUserName:(NSString *)username;

- (void)updateListViewHeaderImageView:(NSString *)backgroundImageUrl;

- (void)updateUserAvatarImageView:(NSString *)avatarImageUrl;

- (void)updateUserNameView:(NSString *)username;

- (void)addMoments:(NSArray<PDCMoment *> *)moments withRealodFlag:(BOOL)reload;

- (void)insertMoment:(PDCMoment *)moment atIndex:(NSUInteger)index;

- (void)updateMoment:(PDCMoment *)moment likeStatus:(BOOL)like;

- (void)deleteMoment:(PDCMoment *)moment;

- (void)addComment:(PDCComment *)comment;

- (void)deleteComment:(PDCComment *)comment ofMoment:(PDCMoment *)moment;

@end
