//
//  PDCComment.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/15.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCUser.h"

@interface PDCComment : NSObject

@property (nonatomic, strong) NSNumber *commentId;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, strong) NSNumber *createTimestamp;
@property (nonatomic, copy) NSString *createTimeStr;
@property (nonatomic, copy) NSString *avatarImageUrl;
@property (nonatomic, assign) BOOL canDel;
@property (nonatomic, assign) BOOL showAllFlag;

@property (nonatomic, strong) PDCUser *createUser;
@property (nonatomic, strong) PDCComment *parentComment;

- (NSAttributedString *)getRealShowAttributeText;

@end
