//
//  PDCCommentCell.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/10/13.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCComment.h"

@interface PDCCommentCell : UITableViewCell

@property (nonatomic, assign) BOOL showTopLine;

- (void)bindModel:(PDCComment *)comment;

+ (CGFloat)heightForModel:(PDCComment *)comment withShowTopLine:(BOOL)showTopLine;

@end
