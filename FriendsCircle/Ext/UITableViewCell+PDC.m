//
//  UITableViewCell+PDC.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "UITableViewCell+PDC.h"

@implementation UITableViewCell (PDC)

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *ID = nil;
    if (ID == nil) {
        ID = [NSString stringWithFormat:@"%@_cellId", NSStringFromClass(self)];
    }
    id cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

@end
