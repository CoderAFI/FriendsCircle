//
//  NSString+PDC.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/16.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (PDC)

- (CGSize)sizeWithFont:(UIFont *)font;

- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)csize;

- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)csize lineBreakMode:(NSLineBreakMode)lineBreakMode;

- (CGSize)sizeWithMaxWidth:(CGFloat)maxWidth font:(UIFont *)font numberOfLines:(NSUInteger)numberOfLines;

- (NSDate *)dateOfFormat:(NSString *)format;

@end
