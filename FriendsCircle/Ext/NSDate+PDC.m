//
//  NSDate+PDC.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/3.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "NSDate+PDC.h"

@implementation NSDate (PDC)

- (NSTimeInterval)timestamp {
    return [self timeIntervalSince1970] * 1000;
}

+ (NSString *)dateStringFromTimestamp:(NSTimeInterval)timestamp withFormat:(NSString *)format {
    NSTimeInterval timeInterval = timestamp / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:format];
    NSString *dateString = [dateformatter stringFromDate:date];
    return dateString;
}

@end
