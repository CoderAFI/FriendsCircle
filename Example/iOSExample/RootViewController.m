//
//  RootViewController.m
//  iOSExample
//
//  Created by 孙燕飞 on 2017/11/18.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "RootViewController.h"
#import <Masonry/Masonry.h>
#import <FriendsCircle/FriendsCircle.h>
#import "DiscoveryCoordinator.h"

#define UIColorFromRGB(rgbHex) \
[UIColor colorWithRed:((float)((rgbHex & 0xFF0000) >> 16))/255.0 green:((float)((rgbHex & 0xFF00) >> 8))/255.0 blue:((float)(rgbHex & 0xFF))/255.0 alpha:1.0]

@interface RootViewController ()

@property (nonatomic, strong) DiscoveryCoordinator *discoveryCoordinator;

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";
    
    UIButton *goDiscoveryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [goDiscoveryBtn setTitle:@"Discovery" forState:UIControlStateNormal];
    goDiscoveryBtn.backgroundColor = UIColorFromRGB(0x323137);
    [goDiscoveryBtn addTarget:self action:@selector(navigateToDiscovery) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:goDiscoveryBtn];
    
    [goDiscoveryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self.view).with.inset(30);
        make.top.equalTo(self.view).offset(100);
        make.height.equalTo(@60);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)navigateToDiscovery {
    
     [PDCLocalizeControl setUserlanguage:@"en"];
    
    PDCMomentsViewController *momentViewController = [PDCMomentsViewController new];
    momentViewController.showMode = AllFriends;
    [self.navigationController pushViewController:momentViewController animated:YES];
    self.discoveryCoordinator = [[DiscoveryCoordinator alloc] initWithMomentsViewController:momentViewController];
}

@end
