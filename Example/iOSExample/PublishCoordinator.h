//
//  PublishCoordinator.h
//  iOSExample
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FriendsCircle/FriendsCircle.h>

@interface PublishCoordinator : NSObject

- (instancetype)initWithPublishViewController:(PDCPublishViewController *)publishViewController;

@end
