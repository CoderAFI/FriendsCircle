//
//  main.m
//  iOSExample
//
//  Created by CoderAFI on 11/16/2017.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
